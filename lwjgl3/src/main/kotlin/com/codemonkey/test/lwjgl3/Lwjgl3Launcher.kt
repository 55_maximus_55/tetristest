@file:JvmName("Lwjgl3Launcher")

package com.codemonkey.test.lwjgl3

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import com.codemonkey.test.Application

/** Launches the desktop (LWJGL3) application. */
fun main() {
    Lwjgl3Application(Application(), Lwjgl3ApplicationConfiguration().apply {
        setTitle("TestGame")
        setWindowedMode(640, 480)
        setWindowIcon(*(arrayOf(128, 64, 32, 16).map { "libgdx$it.png" }.toTypedArray()))
    })
}
