@file:JvmName("HeadlessLauncher")

package com.codemonkey.test.headless

import com.badlogic.gdx.backends.headless.HeadlessApplication
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration
import com.codemonkey.test.Application

/** Launches the headless application. Can be converted into a server application or a scripting utility. */
fun main() {
    HeadlessApplication(Application(), HeadlessApplicationConfiguration().apply {
        // When this value is negative, Application#render() is never called:
        updatesPerSecond = -1
    })
}
